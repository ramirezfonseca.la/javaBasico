package com.luis.dibuja;

import java.awt.Graphics;

import javax.swing.JPanel;

public class Figuras extends JPanel {

	//var con la cual el usuario eligira que figura quiere dibujar 
	private int opcion = 0;
	
	//el constructor va a establecer la opcion de usuario
	public Figuras(int opcionUser) {
		this.opcion = opcionUser;
	}
	
	// este metodo dibujara un acascada de figuras, iniciando desde la esquina superior izquierda
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		for(int i = 0;i<=10;i++) {
			
			   
            switch(opcion){
                case 1: //dibuja rect�ngulos
                    int xRect = 10 + i * 5;
                    int yRect = 10 + i * 5;
                    int anchoRect = 50 + i * 5;
                    int altoRect = 50 + i * 5;
                    g.drawRect(xRect, yRect, anchoRect, altoRect);
                    break;
                case 2: //dibuja �valos
                    int xOval = 10 + i * 10;
                    int yOval = 10 + i * 10;
                    int anchoOval = 35 + i * 10;
                    int altoOval = 35 + i * 10;
                    g.drawOval(xOval, yOval, anchoOval, altoOval);
                    break;
            }
		}
	}
	
	
	
}
