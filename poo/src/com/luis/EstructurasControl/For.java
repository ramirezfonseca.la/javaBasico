package com.luis.EstructurasControl;

public class For {

	public static void main(String[] args) {
		
		for(int i = 0;i<=10;i++) {
			System.out.println("contando\t"+i);
		}

	}

}

/*
 * operadores de asignación compuesto
 * 
 * c = c + 3; es igual a decir c += 3;
 *                             c -= 3;
 *                             c *= 3;
 *                             c /= 3;
 * 
 * */
