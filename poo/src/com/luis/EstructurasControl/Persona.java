package com.luis.EstructurasControl;

import javax.swing.JOptionPane;

public class Persona {

	private String nombre;
	private String nacionalidad;
	private double altura;
	private int edad;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getNacionalidad() {
		return nacionalidad;
	}
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(double altura) {
		if(altura>3) {
			
			JOptionPane.showMessageDialog(null,"Altura Invalidad");
			
		}else {
			this.altura = altura;
		}
		
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
}
