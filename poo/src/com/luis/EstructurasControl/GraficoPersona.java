package com.luis.EstructurasControl;

import javax.swing.JOptionPane;

public class GraficoPersona {

	public static void main(String[] args) {
		
		String nombre = JOptionPane.showInputDialog("Ingresa tu nombre");
		String nacionalidad = JOptionPane.showInputDialog("Ingresa tu Nacionalidad");
		String altura = JOptionPane.showInputDialog("Ingresa tu altura");
		String edad = JOptionPane.showInputDialog("Ingresa tu edad");
		
		//convertimos los String al tipo de dato para realizar la operacion
		
		double alt = Double.parseDouble(altura);
		int ed = Integer.parseInt(edad);
		
		//realizamos el seteo de los datos de la clase Persona
		
		Persona persona = new Persona();
		
		persona.setNombre(nombre);
		persona.setNacionalidad(nacionalidad);
		persona.setAltura(alt);
		persona.setEdad(ed);
		
		JOptionPane.showMessageDialog(null, "Nombre:"+persona.getNombre());
		JOptionPane.showMessageDialog(null, "Nacionalidad:"+persona.getNacionalidad());
		JOptionPane.showMessageDialog(null, "Altura:"+persona.getAltura());
		JOptionPane.showMessageDialog(null, "Edad:"+persona.getEdad());
	}

}
