package com.luis.EstructurasControl;
import javax.swing.JOptionPane;

public class DoWhile {

	public static void main(String[] args) {
     
		int valor = 0;
		String val = "";
		
		do {
			val = JOptionPane.showInputDialog(null, "Ingrese un valor entre 0 y 999");
			
			valor = Integer.parseInt(val);
			
			if(valor>=100) {
				JOptionPane.showMessageDialog(null, "tiene 3 digito");
			}else {
				if(valor>=10) {
					JOptionPane.showMessageDialog(null, "tiene 2 digito");
				}else {
					JOptionPane.showMessageDialog(null, "tiene 1 digito");
				}
			}
			
			
		} while (valor !=0 );
        
	}

}
