package com.luis.basico;

public class Sobrecargados {

	//Clase que muestra como funcionan los metodos sobrecargados
	
	 public void sumar(){
	        System.out.println("Este m�todo no recibe par�metros");
	    }
	    
	    public void sumar(int x, int y){
	        System.out.println("Este m�todo recibe dos enteros");
	    }
	    
	    public void sumar(int x){
	        System.out.println("Este m�todo recibe un entero");
	    }
	    
	    public void sumar(double x){
	        System.out.println("Este m�todo recibe un decimal");
	    }
	    
	    public static void main(String[] args) {
	        Sobrecargados sobreC = new Sobrecargados();
	        
	        sobreC.sumar(2.0);
	    }
	    
}
