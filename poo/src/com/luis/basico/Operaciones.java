package com.luis.basico;

import java.util.Scanner;

public class Operaciones {
	
	 /**
     * Este m�todo va a realizar la suma de dos n�meros
     */
    public int sumar(int numero1, int numero2){
        int sumar = numero1 + numero2;
        return sumar;
    }
    
    /**
     * Este m�todo va a realizar la resta de dos n�meros
     */
    public int restar(int numero1, int numero2) {
        int restar = numero1 - numero2;
        return restar;
    }
    
    /**
     * Este m�todo va a realizar la multiplicaci�n de dos n�meros
     */
    public int multiplicar(int numero1, int numero2) {
        int multiplicar = numero1 * numero2;
        return multiplicar;
    }
    
    /**
     * Este m�todo va a realizar la divisi�n de dos n�meros
     */
    public int dividir(int numero1, int numero2){
        int dividir = numero1 - numero2;
        return dividir;
    }
    
    public void imprimir(int suma, int resta, int multiplicacion, int division){
        System.out.println("La suma es: " + suma);
        System.out.println("La resta es: " + resta);
        System.out.println("La multiplicacion es: " + multiplicacion);
        System.out.println("La division es: " + division);
    }
    
    public static void main(String [] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Introduzca el primer n�mero: ");
        int n1 = sc.nextInt();
        System.out.println("Introduzca el segundo n�mero: ");
        int n2 = sc.nextInt();
        
        Operaciones op = new Operaciones();
        int suma = op.sumar(n1, n2);
        int resta = op.restar(n2, n2);
        int multiplicacion = op.multiplicar(n1, n2);
        int division = op.dividir(n1, n2);
        
        op.imprimir(suma, resta, multiplicacion, division);
        
        
        
      
    }

}
