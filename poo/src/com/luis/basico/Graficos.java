package com.luis.basico;

import javax.swing.JOptionPane;

public class Graficos {
	//Esta clase crea una calculadora gr�fica utilizando los m�todos de la clase OperacionesGraficas
	
	   public static void main(String[] args) {
	        String n1 = JOptionPane.showInputDialog("Introduzca el primer n�mero");
	        String n2 = JOptionPane.showInputDialog("Introduzca el segundo n�mero");
	        
	        double num1 = Double.parseDouble(n1);
	        double num2 = Double.parseDouble(n2);
	        
	        OperacionesGraficas op = new OperacionesGraficas();
	        
	        double suma = op.sumar(num1, num2);
	        double resta = op.restar(num1, num2);
	        double mult = op.multiplicar(num1, num2);
	        double div = op.division(num1, num2);
	        
	        JOptionPane.showMessageDialog(null, "Suma: " + suma);
	        JOptionPane.showMessageDialog(null, "Resta: " + resta);
	        JOptionPane.showMessageDialog(null, "Multiplicacion: " + mult);
	        JOptionPane.showMessageDialog(null, "Division: " + div);
	    }

}
